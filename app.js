// Créez un formulaire d'inscription. Dans celui-ci l'utilisateur est invité à saisir : nom, prénom, login (ne doit pas déjà être utilisé), sexe (bouton radio), mot de passe (au moins huit caractères dont au moins une majuscule, un caractère spécial et un chiffre), date de naissance, ville, mail (valide), url de son site (valide), ses hobbys (max 200 caractères), son numéro de téléphone (XX.XX.XX.XX.XX) ainsi que sa couleur préférée.
// Bonus : implémenter l'autocomplétion pour la ville.
// Bonus 2: pas besoin de soumettre le formulaire pour contrôler les champs.
// Stocker les données récupérées. Ce peut être l'occasion de jeter un oeil au localStorage introduit par HTML5.
// Créez un formulaire de connexion. L'utilisation saisit un login et un mdp. Si les deux correspondent, on l'autorise à se connecter et on l'accueille avec un message du type : "Bienvenue <prénom> "

// var email = document.getElementById("mail");

// email.addEventListener("input", function (event) {
//   if (email.validity.typeMismatch) {
//     email.setCustomValidity("I expect an e-mail, darling!");
//   } else {
//     email.setCustomValidity("");
//   }
// });

$("#boutonFinal").on("click", function(){
    var annuaire = localStorage.getItem("annuaire");

    if(  annuaire == null ){
         annuaire = "[]";
    }
        annuaire = JSON.parse(annuaire);

    var fiche = {
        nom : $("#nom").val(),
        prenom : $("#prenom").val(),
        identifiant : $("#identifiant").val(),
        sexe1 : $("#sexe1").val(),
        sexe2 : $("#sexe2").val(),
        sexe3 : $("#sexe3").val(),
        motDePasse : $("#motDePasse").val(),
        dateDeNaissance : $("#dateDeNaissance").val(),
        ville : $("#ville").val(),
        email : $("#email").val(),
        url : $("#url").val(),
        msg : $("#msg").val(),
        numeroDeTelephone : $("#numeroDeTelephone").val(),
        couleurPreferee : $("#couleurPreferee").val(),
        
    };
    annuaire.push(fiche);
    localStorage.setItem( "annuaire", JSON.stringify(annuaire));
})



